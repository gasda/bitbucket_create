# Utility to create new repositories at bitbucket.com

### Usage
* Copy bitbucket into a folder in your path like ~/bin/
* Edit the USER and PASS variables to match your bitbucket account
* Then run it from the folder of any new repository
~~~~
/home/user1/my_project $ bitbucket repo_name

~~~~
### Why
I just found myself creating local git repositories and wanted a quick way to push them.

